#!/usr/bin/env python
from jinja2 import Template, FileSystemLoader, Environment
import json
import os
import re


def load_js():
    folder = "media/videos/anim/1080p60/sections/"
    files = os.listdir(folder)
    pat = re.compile("(.+)\.json$")
    jss = {}
    for name in files:
        m = re.search(pat, name)
        if m is None:
            continue
        js = json.load(open(folder + name))
        jss[m.group(1)] = js

    return jss


def index_of(scene, section):
    els = list(filter(lambda x: x[1]["name"] == section, enumerate(jss[scene])))
    if len(els) == 0:
        print("Section " + section + " in scene " + scene + " does not exist")
        exit(1)
    elif len(els) == 1:
        return els[0][0]
    else:
        print("Section " + section + " in scene " + scene + " exists multiple times")
        exit(1)


def video_by_index(scene, i):
    path = jss[scene][i]["video"]
    return (
        """<div class="slide">
            <div class="containerB">
                    <video autoplay muted  id="myVideo">
                        <source src=media/videos/anim/1080p60/sections/"""
        + path
        + """ type="video/mp4">
                    </video>
            </div>
        </div>"""
    )


# Adds a slide with the animation from section "section" of scene "scene"
def manim_single(scene, section):
    i = index_of(scene, section)
    return video_by_index(scene, i)


# Adds a slides with the animations from section "section", starting from the
# section `firstSection` and ending at the section `lastSection`. If
# `firstSection is None`, the first section of the scene is assumed as the
# `firstSection`.  If `lastSection is None`, the last section of the scene is
# assumed as the `lastSection`.
def manim_range(scene, firstSection=None, lastSection=None):
    if firstSection is None:
        first = 0
    else:
        first = index_of(scene, firstSection)
    if lastSection is None:
        last = len(jss[scene]) - 1
    else:
        last = index_of(scene, lastSection)

    return "\n".join(map(lambda i: video_by_index(scene, i), range(first, last + 1)))


jss = load_js()
templateLoader = FileSystemLoader(searchpath="./")
templateEnv = Environment(loader=templateLoader)
template = templateEnv.get_template("source.html")
out = template.render(manim_range=manim_range, manim_single=manim_single)
with open("output.html", "w") as outfile:
    outfile.write(out)
