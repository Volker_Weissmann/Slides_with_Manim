from manim import *


class ExampleScene(Scene):
    def construct(self):
        # Without the extra `self.wait()`'s the animation will stop one frame to early

        self.next_section("sec1")
        circle = Circle(radius=5)
        self.play(Create(circle))
        self.wait()

        self.next_section("sec2")
        rect = Rectangle()
        self.play(Create(rect))
        self.wait()

        self.next_section()
        self.play(Uncreate(circle))
        self.wait()

        self.next_section()
        self.play(Uncreate(rect))
        self.wait()
