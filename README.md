# Slides with Manim

Example how to use Manim in slides for a presentation.

The manim animations are found in `anim.py`.
The main code for the slides is found in `slides.html`.

To build it, run:
```sh
manim --verbosity WARNING --save_sections --write_all anim.py
./genhtml.py
```
`output.html` will contain the presentation.
Use right/left arrow-keys to go to the next/previous slide.
